import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angularRoutingTest';
  events = {
    testChange: (event) => this.updateRouteData(event)
  };

  updateRouteData(event) {
    if (event.prop === 'testChange') {
      this.title = event.data;
    }
  }
}
