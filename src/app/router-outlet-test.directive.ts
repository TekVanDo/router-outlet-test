import {
  Attribute,
  ChangeDetectorRef,
  ComponentFactoryResolver,
  Directive, EventEmitter, Injector, Input, Output,
  ViewContainerRef
} from '@angular/core';
import { ActivatedRoute, ChildrenOutletContexts, RouterOutlet } from '@angular/router';

@Directive({ selector: 'router-outlet-test' })
export class RouterOutletTestDirective extends RouterOutlet {
  @Input() set data(data) {
    this._data = data;
    try {
      if (this.component) {
        this.component['test'] = this._data;
      }
    } catch (e) {

    }
  }

  @Input() eventsData;

  @Output() componentEvents = new EventEmitter();
  _data;

  constructor(
    parentContexts: ChildrenOutletContexts, location: ViewContainerRef,
    resolver: ComponentFactoryResolver, @Attribute('name') name: string,
    changeDetector: ChangeDetectorRef) {
    super(parentContexts, location, resolver, name, changeDetector);
  }

  activateWith(activatedRoute: ActivatedRoute, resolver: ComponentFactoryResolver | null) {
    super.activateWith(activatedRoute, resolver);
    this.component['test'] = this._data;
    const props = Object.getOwnPropertyNames(this.component);
    props.forEach((prop) => {
      if (this.component[prop].constructor === EventEmitter) {
        this.component[prop].subscribe((data) => {
          this.componentEvents.emit({ prop, data });
          if (this.eventsData[prop]) {
            this.eventsData[prop](data);
          }
        });
      }
    });
  }
}
