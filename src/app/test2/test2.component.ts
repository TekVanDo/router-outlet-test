import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {
  @Input() test = '123';
  @Output() testChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  emit(data) {
    this.testChange.emit(data);
  }
}
